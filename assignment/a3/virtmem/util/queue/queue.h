/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   queue.h
 * Author: panteparak
 *
 * Created on February 22, 2018, 2:25 AM
 */

#ifndef QUEUE_H
#define QUEUE_H

#ifndef CQUEUE_H
#define CQUEUE_H

#include <stdio.h>
#include <stdlib.h>

typedef void (*Element)(void *);

struct Job {
    Element *element;
    struct Job *next;
};

struct JobQueue {
    int size;
    struct Job* head;
    struct Job* tail;
};

/* Push a word to the back of this queue
 * You must keep a *COPY* of the word.
 * If q is NULL, allocate space for it here
 */
void push(struct JobQueue** q, Element *element);

/* Returns the data at the front of the queue
 * and remove it from the queue as well.
 * If q is empty, return NULL
 */
struct Job* pop(struct JobQueue *q);

/* Checks if the queue is empty */
int isEmpty(struct JobQueue *q);

/* Returns the size of the queue
 * If the queue is empty, return 0 else size.
 */
int size(struct JobQueue *q);

/* Deallocates all items in the queue */
void delete(struct JobQueue *q);

#endif

#endif /* QUEUE_H */

