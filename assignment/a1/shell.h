
#ifndef _SHELLH_
#define _SHELLH_

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <sys/termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#define MAX_ARG_SIZE     1024
//const char SEPARATOR = ' ';


/* A process is a single process.  */
typedef struct process {
    struct process *next;       /* next process in pipeline */
    char **argv;                /* for exec */
    int argc;                   /* counting argv */
    pid_t pid;                  /* process ID */
    char completed;             /* true if process has completed */
    char stopped;               /* true if process has stopped */
    int status;                 /* reported status value */
} process;

/* A job is a pipeline of processes.  */
typedef struct job {
    struct job *next;           /* next active job */
    char *infile, *outfile;      /* file redirection */
    process *processes;     /* list of processes in this job */
    pid_t pgid;                 /* process group ID */
    char notified;              /* true if user told about stopped job */
    struct termios tmodes;      /* saved terminal modes */
    int stdin, stdout, stderr;  /* standard i/o channels */
    int id;
    int foreground;
} job;

pid_t shell_pgid;
struct termios shell_tmodes;
int shell_terminal;
int shell_is_interactive;
struct job *first_job;
int exitcode;

void init_shell();

void launch_process(struct process *p, pid_t pgid, int infile, int outfile, int errfile, int foreground);

void launch_job(struct job *j, int foreground);

void put_job_in_foreground(struct job *j, int cont);

void put_job_in_background(struct job *j, int cont);

void printJob(struct job *j, const char *status);

void wait_for_job(struct job *j);

int mark_process_status(pid_t pid, int status);

int job_is_completed(struct job *j);

int job_is_stopped(struct job *j);

struct job *create_job(void);

struct process *create_process(void);

void update_status(void);

void free_process(struct process *p);

void free_job(struct job *j);

struct job *find_job_id(int id);

void do_job_notification(void);

void setRunningJob(struct job *j);

void continueJob(struct job *j, int foreground);

int launchBuiltIn(struct process *p, int infile, int outfile, int errfile);
#endif
