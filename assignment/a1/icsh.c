#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <ctype.h>
#include "shell.h"


job *first_job = NULL;
int id = 1;

struct Command {
    char **cmd;
    char **args;
    int *argc;
    int *pipein;
    int *pipeout;
    int *background;
};

struct job *create_job() {
    struct job *job = (struct job *) malloc(sizeof(struct job));
    job->id = id++;
    job->pgid = 0;
    job->notified = 0;
    job->foreground = 1;
    job->processes = NULL;
    job->infile = NULL;
    job->outfile = NULL;
    job->stdin = STDIN_FILENO;
    job->stdout = STDOUT_FILENO;
    job->stderr = STDERR_FILENO;
    job->next = NULL;
    return job;
}

struct process *create_process() {
    struct process *p = (struct process *) malloc(sizeof(struct process));
    p->next = NULL;
    p->argv = NULL;
    p->argc = 0;
    p->completed = 0;
    p->stopped = 0;

    return p;
}

char* trim(char *str) {
    char *end;
    while(isspace((unsigned char) * str)) str++;

    if(*str == 0)
        return str;

    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;
    *(end+1) = '\0';
    return str;
}

int count_delimiter(char *input){
    input = trim(input);
    int c = 1;
    for (int i = 0; i < (int)strlen(input); ++i) {
        c = (input[i] == ' ' ? c + 1 : c);
    }
    return strlen(input) > 0 ? c : 0;
}

void cmd_parser(char *input, struct Command **command){
    int delim = count_delimiter(input);

    char *token;

    if (delim){
        token = strtok(input, " ");
        token = trim(token);
        *command = (struct Command*)malloc(sizeof(struct Command));
        (*command)->pipein = NULL;
        (*command)->pipeout = NULL;
        (*command)->pipein = NULL;
        (*command)->background = NULL;

        (*command)->argc = (int *)malloc(sizeof(int));
        (*command)->cmd = (char**)malloc((size_t) (delim + 1) * sizeof(char*));

        for (int i = 0; i <= delim; ++i) (*command)->cmd[i] = NULL;


        *(*command)->argc = 0;

        (*command)->args = (char **)malloc(sizeof(char *) * delim);
        (*command)->argc = (int *)malloc(sizeof(int));
        *((*command)->argc) = delim;


        (*command)->args[0] = (char *) malloc(sizeof(char) * MAX_ARG_SIZE);
        (*command)->cmd[0] = (char *) malloc(MAX_ARG_SIZE * sizeof(char));
        strncpy((*command)->args[0], token, MAX_ARG_SIZE);
        strncpy((*command)->cmd[0], token, MAX_ARG_SIZE);

        if (strchr(token, '&') && (*command)->background == NULL){
            (*command)->background = (int *) malloc(sizeof(int));
            *(*command)->background = 0;
        }
    }

    if (delim > 1){
        for (int i = 1, j = 1; i < *(*command)->argc && (token = strtok(NULL, " ")); ++i) {
            token = trim(token);

            (*command)->args[i] = (char *) malloc(sizeof(char) * MAX_ARG_SIZE);
            strncpy((*command)->args[i], token, MAX_ARG_SIZE);

            if ((!strcmp((*command)->args[i], "&") || strchr((*command)->args[i], '&')) && (*command)->background == NULL){ // def& should be true
                (*command)->background = (int *) malloc(sizeof(int));
                *(*command)->background = i;
                continue;
            } else if (((!strcmp((*command)->args[i], ">") || strchr((*command)->args[i], '>')) && i + 1 < *(*command)->argc)){
                (*command)->pipeout = (int *) malloc(sizeof(int));
                *(*command)->pipeout = i + 1;
                continue;
            } else if ((!strcmp((*command)->args[i], "<") || strchr((*command)->args[i], '<')) && i + 1 < *(*command)->argc) {
                (*command)->pipein = (int *) malloc(sizeof(int));
                *(*command)->pipein = i + 1;
                continue;
            } else{
                if (((*command)->pipein != NULL && *(*command)->pipein == i) || ((*command)->pipeout != NULL && *(*command)->pipeout == i)) continue;
                (*command)->cmd[j] = (char *) malloc(sizeof(char) * MAX_ARG_SIZE);
                strncpy((*command)->cmd[j++], token, MAX_ARG_SIZE);
            }
        }
    }
}

void construct(struct job** j, struct Command *command){
    struct job* job = create_job();
    struct process *p = create_process();
    job->processes = p;
    p->argc = *command->argc;
    p->argv = command->cmd;
    job->foreground = (command->background == NULL ? 1 : 0);

    if (command->pipein != NULL){
        char *in = command->args[*command->pipein];
        job->infile = in;
    }

    if (command->pipeout != NULL){
        char *out = command->args[*command->pipeout];
        job->outfile = out;
    }

    *j = job;
}

int main(){
    char cmd[MAX_ARG_SIZE];
    int cont = 0;

    struct Command* command;
    init_shell();
    while (!cont) {
        printf("icsh> ");
        if (fgets(cmd, MAX_ARG_SIZE, stdin) != NULL && strlen(cmd) > 0){
            cmd_parser(cmd, &command);
        }
        if (command != NULL){
            struct job* j;
            construct(&j, command);
            if (first_job == NULL){
                first_job = j;

            }else {
                struct job *t;
                for(t = first_job; t->next; t = t->next);
                t->next = j;
            }
            launch_job(j, j->foreground);
            do_job_notification();
            command = NULL;
        }
    }

    return EXIT_SUCCESS;
}
