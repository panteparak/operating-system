
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <sys/termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include "shell.h"


char *builtinKeyMapper[] = {"echo", "jobs", "fg", "bg", "exit"};

int builtinCount() {
    return 5;
}



int command_jobs(struct process *p, int infile, int outfile, int errfile) {
    if (p->argv[1]) {
        for (int i = 1; p->argv[i]; ++i) {
            int id = atoi(p->argv[i]);
            job *j = find_job_id(id);

            if (j && !job_is_completed(j)) {
                if (job_is_stopped(j)) {
                    fprintf(stdout, "%d %ld Stopped\n", j->id, (long) j->pgid);
                } else {
                    fprintf(stdout, "%d %ld Running\n", j->id, (long) j->pgid);
                }
            } else{
                fprintf(stderr, "jobs - %s: no such job\n", p->argv[i]);
            }

        }
        return 0;
    }
    update_status();

    for (job *j = first_job; j; j = j->next) {
        if (!job_is_completed(j) && j->id) {
            if (job_is_stopped(j)) {
                fprintf(stdout, "%d) %ld Stopped\n", j->id, (long) j->pgid);
            } else {
                fprintf(stdout, "%d) %ld Running\n", j->id, (long) j->pgid);
            }
        }
    }

    update_status();
    return 0;
}

int command_echo(struct process *p, int infile, int outfile, int errfile) {
    if (!p->argv[1]) {
        return 0;
    }
    if (!strcmp(p->argv[1], "$?")) {
        printf("%d\n", exitcode);
        return 0;
    }
    for (int i = 1; p->argv[i]; ++i) {
        printf("%s ", p->argv[i]);
    }
    printf("\n");
    return 0;
}

int command_exit(struct process *p, int infile, int outfile, int errfile) {
    update_status();
    exit(0);
}

int command_fg(struct process *p, int infile, int outfile, int errfile) {
    if (p->argv[1]) {

        for (int i = 1; p->argv[i]; ++i) {
            int id = atoi(&(p->argv[i][1]));
            job *j = find_job_id(id);

            if (j && !job_is_completed(j) && job_is_stopped(j)) {
                continueJob(j, 1);
            } else if (j && !j->foreground) {
                put_job_in_foreground(j, 1);
            } else{
                fprintf(stderr, "fg: %s : no such job\n", p->argv[i]);
            }
        }

        return 0;
    }

    job *last = NULL;
    update_status();

    for (job *j = first_job; j; j = j->next) {
        if (!job_is_completed(j) && j->id && job_is_stopped(j)) {
            last = j;
        }
    }

    if (last){
        continueJob(last, 1);
    } else{
        fprintf(stderr, "fg: current: no such job\n");
    }

    update_status();

    return 0;
}

int command_bg(struct process *p, int infile, int outfile, int errfile) {
    if (p->argv[1]) {
        for (int i = 1; p->argv[i]; ++i) {
            int id = atoi(&(p->argv[i][1]));
            job *j = find_job_id(id);
            if (j && (!job_is_completed(j) && job_is_stopped(j))) {
                continueJob(j, 0);
            } else if (j && j->foreground) {
                put_job_in_background(j, 1);
            } else {
                fprintf(stderr, "bg: %s : no such job\n", p->argv[i]);
            }
        }
        return 0;
    }

    job *last = NULL;
    update_status();

    for (job *j = first_job; j; j = j->next) {
        if (!job_is_completed(j) && j->id && job_is_stopped(j)) {
            last = j;
        }
    }
    if (last){
        continueJob(last, 0);
    } else{
        fprintf(stderr, "bg: current: no such job\n");
    }
    update_status();

    return 0;
}

int (*builtinFunctionMapper[])(struct process *p, int infile, int outfile, int errfile) = {
        &command_echo, &command_jobs, &command_fg, &command_bg, &command_exit
};

int launchBuiltIn(struct process *p, int infile, int outfile, int errfile) {
    int n = builtinCount();

    for (int i = 0; i < n; i++) {
        if (!strcmp(builtinKeyMapper[i], p->argv[0])) {
            return (*builtinFunctionMapper[i])(p, infile, outfile, errfile);
        }
    }
    return -1;
}

/* List of all functions to be used later */
void init_shell() {

    /* See if we are running interactively.  */
    shell_terminal = STDIN_FILENO;
    shell_is_interactive = isatty(shell_terminal);
    if (shell_is_interactive) {

        /* Loop until we are in the foreground.  */
        while (tcgetpgrp(shell_terminal) != (shell_pgid = getpgrp()))
            kill(-shell_pgid, SIGTTIN);

        /* Ignore interactive and job-control signals.  */
        signal(SIGINT, SIG_IGN);
        signal(SIGQUIT, SIG_IGN);
        signal(SIGTSTP, SIG_IGN);
        signal(SIGTTIN, SIG_IGN);
        signal(SIGTTOU, SIG_IGN);

        /* Put ourselves in our own process group.  */
        shell_pgid = getpid();
        if (setpgid(shell_pgid, shell_pgid) < 0) {
            perror("Couldn't put the shell in its own process group");
            exit(1);
        }

        /* Grab control of the terminal.  */
        tcsetpgrp(shell_terminal, shell_pgid);

        /* Save default terminal attributes for shell.  */
        tcgetattr(shell_terminal, &shell_tmodes);
    }
}

void launch_process(struct process *p, pid_t pgid,
                    int infile, int outfile, int errfile,
                    int foreground) {
    pid_t pid;

    if (shell_is_interactive) {
        /* Put the process into the process group and give the process group
           the terminal, if appropriate.
           This has to be done both by the shell and in the individual
           child processes because of potential race conditions.  */
        pid = getpid();
        if (pgid == 0) {
            pgid = pid;
        }
        setpgid(pid, pgid);
        if (foreground) { tcsetpgrp(shell_terminal, pgid); }

        /* Set the handling for job control signals back to the default.  */
        signal(SIGINT, SIG_DFL);
        signal(SIGQUIT, SIG_DFL);
        signal(SIGTSTP, SIG_DFL);
        signal(SIGTTIN, SIG_DFL);
        signal(SIGTTOU, SIG_DFL);
        signal(SIGCHLD, SIG_DFL);
    }

    /* Set the standard input/output channels of the new process.  */
    if (infile != STDIN_FILENO) {
        dup2(infile, STDIN_FILENO);
        close(infile);
    }
    if (outfile != STDOUT_FILENO) {
        dup2(outfile, STDOUT_FILENO);
        close(outfile);
    }
    if (errfile != STDERR_FILENO) {
        dup2(errfile, STDERR_FILENO);
        close(errfile);
    }

    /* Exec the new process.  Make sure we exit.  */

    execvp(p->argv[0], p->argv);
    perror("execvp");
    exit(1);
}

void launch_job(job *j, int foreground) {
    struct process *p;
    pid_t pid;
    int mypipe[2], infile, outfile;
    if (j->infile) {
        j->stdin = open(j->infile, O_RDONLY);
        if (j->stdin < 0) {
            printf("Can't open infile\n");
            perror(j->infile);
            exit(1);
        }
    }
    if (j->outfile) {
        j->stdout = open(j->outfile, O_WRONLY | O_CREAT | O_TRUNC, 0700);
        if (j->stdout < 0) {
            printf("Can't open outfile\n");
            perror(j->outfile);
            exit(1);
        }
    }
    infile = j->stdin;
    for (p = j->processes; p; p = p->next) {
        /* Set up pipes, if necessary.  */
        if (p->next) {
            if (pipe(mypipe) < 0) {
                perror("pipe");
                exit(1);
            }
            outfile = mypipe[1];
        } else
            outfile = j->stdout;
        if (!launchBuiltIn(p, infile, outfile, j->stderr)) {
            p->completed = 1;
        } else {
            /* Fork the child processes.  */
            pid = fork();
            if (pid == 0) {
                /* This is the child process.  */
                launch_process(p, j->pgid, infile,
                               outfile, j->stderr, foreground);
            } else if (pid < 0) {
                /* The fork failed.  */
                perror("fork");
                exit(1);
            } else {
                /* This is the parent process.  */
                p->pid = pid;
                if (shell_is_interactive) {
                    if (!j->pgid) {
                        j->pgid = pid;
                        j->id++;
                    }
                    setpgid(pid, j->pgid);
                }
            }
        }
        /* Clean up after pipes.  */
        if (infile != j->stdin)
            close(infile);
        if (outfile != j->stdout)
            close(outfile);
        infile = mypipe[0];
    }

    if (!shell_is_interactive)
        wait_for_job(j);
    else if (foreground)
        put_job_in_foreground(j, 0);
    else {
        put_job_in_background(j, 0);
        printJob(j, "background");
    }
}

void put_job_in_foreground(struct job *j, int cont) {
    j->foreground = 1;

    /* Put the job into the foreground.  */
    tcsetpgrp(shell_terminal, j->pgid);

    /* Send the job a continue signal, if necessary.  */
    if (cont) {
        tcsetattr(shell_terminal, TCSADRAIN, &j->tmodes);
        if (kill(- j->pgid, SIGCONT) < 0)
            perror("kill (SIGCONT)");
        else {
            printJob(j, "foreground");
        }
    }

    /* Wait for it to report.  */
    wait_for_job(j);

    /* Put the shell back in the foreground.  */
    tcsetpgrp(shell_terminal, shell_pgid);

    /* Restore the shell’s terminal modes.  */
    tcgetattr(shell_terminal, &j->tmodes);
    tcsetattr(shell_terminal, TCSADRAIN, &shell_tmodes);
}

void put_job_in_background(struct job *j, int cont) {
    /* Send the job a continue signal, if necessary.  */
    j->foreground = 0;

    if (cont) {
        if (kill(-j->pgid, SIGCONT) < 0) {
            perror("killed: (SIGCONT)");
        } else {
            printJob(j, "background");
        };
    }
}

void printJob(struct job *j, const char *status) {
    fprintf(stderr, "%d) %ld (%s): %s\n", j->id, (long) j->pgid, status, j->processes->argv[0]);
}

void wait_for_job(struct job *j) {
    pid_t pid;
    do {
        pid = waitpid(-j->pgid, &exitcode, WUNTRACED);
    } while (!mark_process_status(pid, exitcode)
             && !job_is_stopped(j)
             && !job_is_completed(j));
}


int mark_process_status(pid_t pid, int status) {
    struct job *j;
    struct process *p;
    if (pid > 0) {
        /* Update the record for the process.  */
        for (j = first_job; j; j = j->next) {
            for (p = j->processes; p; p = p->next) {
                if (p->pid == pid) {
                    p->status = status;
                    if (WIFSTOPPED (status)) {
                        p->stopped = 1;
                    } else {
                        p->completed = 1;
                        if (WIFSIGNALED (status)) {
                            fprintf(stderr, "%d: Terminated by signal %d.\n",
                                    (int) pid, WTERMSIG (p->status));
                        }
                    }
                    return 0;
                }
            }
        }
        fprintf(stderr, "No child process %d.\n", pid);
        return -1;
    } else if (pid == 0 || errno == ECHILD)
        /* No processes ready to report.  */
        return -1;
    else {
        perror("waitpid");
        return -1;
    }
}

/* Return true if all processes in the job have completed.  */
int job_is_completed(struct job *j) {
    struct process *p;
    for (p = j->processes; p; p = p->next) {
        if (!p->completed)
            return 0;
    }
    return 1;
}

/* Return true if all processes in the job have stopped or completed.  */
int job_is_stopped(struct job *j) {
    struct process *p;

    for (p = j->processes; p; p = p->next) {
        if (!p->completed && !p->stopped) {
            return 0;
        }
    }
    return 1;
}

void update_status() {
    int status;
    pid_t pid;
    do {
        pid = waitpid(-1, &status, WUNTRACED | WNOHANG);
    } while (!mark_process_status(pid, status));
}

void free_process(struct process *p) {
    if (!p->argv) return;
    free(p->argv);
}

void free_job(struct job *j) {
    if (!j) {
        return;
    }
    struct process *p = j->processes;
    while (p) {
        struct process *tmp = p->next;
        free_process(p);
        p = tmp;
    }
}

job *find_job_id(int id) {
    if (id < 1) return NULL;

    for (struct job *j = first_job; j; j = j->next)
        if (j->id == id)
            return j;
    return NULL;
}

void do_job_notification() {
    struct job *j;
    struct job *last;
    struct job *next;

    /* Update status information for child processes.  */
    update_status();

    last = NULL;
    for (j = first_job; j; j = next) {

        next = j->next;

        /* If all processes have completed, tell the user the job has
        completed and delete it from the list of active jobs.  */
        if (job_is_completed(j)) {
            if (!j->foreground) {
                printJob(j, "Done");
            }
            if (last) {
                last->next = next;
            } else {
                first_job = next;
            }
            free_job(j);
        }

            /* Notify the user about stopped jobs,
            marking them so that we won’t do this more than once.  */
        else if (job_is_stopped(j) && !j->notified) {
            printJob(j, "Stopped");
            j->notified = 1;
            last = j;
        } else
            last = j;
    }
}

void setRunningJob(struct job *j) {
    struct process *p;
    for (p = j->processes; p; p = p->next) {
        p->stopped = 0;
    }
    j->notified = 0;
}

void continueJob(struct job *j, int foreground) {
    setRunningJob(j);
    if (foreground == 1) {
        put_job_in_foreground(j, 1);
    } else {
        put_job_in_background(j, 1);
    }
}
