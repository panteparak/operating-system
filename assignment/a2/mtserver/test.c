#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>  
#include <stdio.h>

double time_diff(struct timeval *prior, struct timeval *latter);
void measure_time(struct timeval** start, struct timeval** current);


int main(){
    struct timeval* start, *current;
    while(1){
        measure_time(&start, &current);
    }

}

void measure_time(struct timeval** start, struct timeval** current){
    
    static int count = 0;
    const int SECONDS = 10;
    const int MILIS = SECONDS * 1000;
    
    if (*start == NULL){
        *start = (struct timeval *) malloc(sizeof(struct timeval));
        *current = (struct timeval *) malloc(sizeof(struct timeval));   
        gettimeofday(*start, NULL);
    }
    
    gettimeofday(*current, NULL);

    if (time_diff(*start, *current) >= MILIS){
        // printf("Measured for thread in pool %d - workload %d\n", args->thread_num, args->loop_num);
        printf("  throughput %.3f\n", (count/time_diff(*start, *current)));
        count = 0;
        gettimeofday(*start, NULL);
    }else{
        count++;
    }
}


double time_diff(struct timeval *prior, struct timeval *latter) {
    return (double) (latter->tv_usec - prior->tv_usec) / 1000.0L + 
            (double) (latter->tv_sec - prior->tv_sec) * 1000.0L;
}