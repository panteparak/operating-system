/**
 * threadpool.c
 *
 * This file will contain your implementation of a threadpool.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#include "threadpool.h"
#include "queue.h"

#define RUNNING (2)
#define WAITING (1)
#define KILLED (-1)
#define STARTING (0)

struct Thread {
    int thread_id;
    int status;
    pthread_t pthread;
};

typedef struct _threadpool_st {
    int size, available_thread;
    pthread_mutex_t mutex_queue;
    pthread_cond_t signal_has_job;
    pthread_cond_t signal_finish_job;
    int shutdown;
    struct Thread *threads;
    struct JobQueue *queue;
} _threadpool;

struct WorkerArgs {
    int thread_id;
    _threadpool *pool;
};

void thread_debug(const int thread_id, char * text) {
    if (thread_id >= 0)
        fprintf(stdout, " [Thread-%d] - %s\n", thread_id, text);
    else
        fprintf(stdout, " [**main**] - %s\n", text);
}

void *worker_thread(void *arg) {
    struct WorkerArgs *args = (struct WorkerArgs *) arg;

    _threadpool *pool = args->pool;
    struct JobQueue *queue = pool->queue;
    int thread_id = args->thread_id;
    struct Job *job = NULL;
    
    pool->available_thread++;
//    pthread_cond_signal(&pool->signal_finish_job);

    while (1) {
        pool->threads[thread_id].status = WAITING;
        
        if (pool->shutdown){
            pthread_exit(NULL);
            break;
        }
        
        pthread_mutex_lock(&pool->mutex_queue);
//        printf("queue size : %d\n", size(queue));
        while (isEmpty(queue)) {
            pthread_cond_wait(&pool->signal_has_job, &pool->mutex_queue);
        }
       
        job = pop(queue);;
        pthread_mutex_unlock(&pool->mutex_queue);

        pool->threads[thread_id].status = RUNNING;

        (job->fn) (job->arg);
        free(job);

        pthread_mutex_lock(&pool->mutex_queue);
        pool->available_thread++;
        pthread_cond_signal(&pool->signal_finish_job);
        pthread_mutex_unlock(&pool->mutex_queue);
    }
}

threadpool create_threadpool(int num_threads_in_pool) {
    _threadpool *pool;

    if ((num_threads_in_pool <= 0) || (num_threads_in_pool > MAXT_IN_POOL)) {
        return NULL;
    }

    pool = (_threadpool *) malloc(sizeof (_threadpool));

    if (pool == NULL) {
        fprintf(stderr, "Out of memory creating a new threadpool!\n");
        return NULL;
    }
    if ((pthread_mutex_init(&pool->mutex_queue, NULL))) {
        fprintf(stderr, "Failed to initialise queue_mutex\n");
        return NULL;
    }
    if (pthread_cond_init(&pool->signal_finish_job, NULL)) {
        fprintf(stderr, "init error: signal_empty_queue\n");
        return NULL;
    }
    if (pthread_cond_init(&pool->signal_has_job, NULL)) {
        fprintf(stderr, "init error: signal_full_queue\n");
        return NULL;
    }

    pool->size = num_threads_in_pool;
    pool->threads = (struct Thread *) malloc(sizeof (struct Thread) * num_threads_in_pool);
    pool->queue = (struct JobQueue *) malloc(sizeof (struct JobQueue));

    if (pool->threads == NULL) {
        fprintf(stderr, "Out of memory creating child thread metadata!\n");
        return NULL;
    }

    if (pool->queue == NULL) {
        fprintf(stderr, "Out of memory creating pool\n");
        return NULL;
    }

    for (int i = 0; i < num_threads_in_pool; i++) {
        pool->threads[i].thread_id = i;
        pool->threads[i].status = STARTING;

        struct WorkerArgs *worker_args = malloc(sizeof (struct WorkerArgs));

        if (worker_args == NULL) {
            fprintf(stderr, "Out of memory creating child thread arguments metadata!\n");
            return NULL;
        }

        worker_args->pool = pool;
        worker_args->thread_id = i;
        int status = pthread_create(&(pool->threads[i].pthread), NULL, worker_thread, worker_args);

        switch (status) {
            case EPERM:
                fprintf(stderr, "The caller does not have appropriate permission to set the required scheduling parameters or scheduling policy.");
                return NULL;
            case EINVAL:
                fprintf(stderr, "The value specified by attr is invalid.");
                return NULL;
            case EAGAIN:
                fprintf(stderr, "The system lacked the necessary resources to create another thread, or the system-imposed limit on the total number of threads in a process [PTHREAD_THREADS_MAX] would be exceeded.");
                return NULL;
        }

        pthread_detach((pool->threads[i].pthread));
    }
    
//    printf("size: %d, available: %d\n", pool->size, pool->available_thread);
    
    return (threadpool) pool;
}

void dispatch(threadpool from_me, dispatch_fn dispatch_to_here, void *arg) {
    if (from_me == NULL) {
        fprintf(stderr, "threadpool is NULL");
        return;
    }
    _threadpool *pool = (_threadpool *) from_me;
    
    
    if (pool->shutdown){
        return;
    }
    
    pthread_mutex_lock(&pool->mutex_queue);
    pool->available_thread--;
    push(&pool->queue, dispatch_to_here, arg);
    
    pthread_cond_signal(&pool->signal_has_job);
    while (pool->available_thread == 0) {
        pthread_cond_wait(&pool->signal_finish_job, &pool->mutex_queue);
    }
    
    pthread_mutex_unlock(&pool->mutex_queue);
    
}

void destroy_threadpool(threadpool destroyme) {
    _threadpool *pool = (_threadpool *) destroyme;
    
    
    pool->shutdown = 1;
    
    pthread_mutex_lock(&pool->mutex_queue);
    while(pool->available_thread != pool->size){
        pthread_cond_wait(&pool->signal_finish_job, &pool->mutex_queue);
    }
    pthread_mutex_unlock(&pool->mutex_queue);
    
    delete(pool->queue);
    pthread_mutex_destroy(&pool->mutex_queue);
    pthread_cond_destroy(&pool->signal_finish_job);
    pthread_cond_destroy(&pool->signal_has_job);
    printf("Destroy\n");
    exit(0);

}
