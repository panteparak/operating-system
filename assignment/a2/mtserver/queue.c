/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "queue.h"

void push(struct JobQueue **q, dispatch_fn fn, void *arg) {
    struct Job *new_job = malloc(sizeof (struct Job));
    new_job->next = NULL;
    new_job->arg = arg;
    new_job->fn = fn;

    if (*q == NULL) {
        *q = (struct JobQueue *) malloc(sizeof (struct JobQueue));
        (*q)->head = NULL;
        (*q)->tail = NULL;
        (*q)->size = 0;
    }

    if (isEmpty(*q)) {
        (*q)->head = new_job;
        (*q)->tail = new_job;
    } else {
        (*q)->tail->next = new_job;
        (*q)->tail = new_job;
    }

    (*q)->size++;
}

struct Job* pop(struct JobQueue *q) {

    if (isEmpty(q)) {
        return NULL;
    }

    struct Job *node = q->head;

    if (node->next == NULL) {
        q->head = NULL;
        q->tail = NULL;
    } else {
        q->head = q->head->next;
    }
    
    q->size--;
    return node;
}

int isEmpty(struct JobQueue *q) {
    return q == NULL || (q->head == NULL && q->tail == NULL);
}

int size(struct JobQueue *q){
    return isEmpty(q) ? 0 : q->size;
}

void delete(struct JobQueue *q) {
    while (!isEmpty(q)) {
        struct Job *a = pop(q);
        free(a);
    }
}