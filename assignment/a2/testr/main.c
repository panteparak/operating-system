#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>

long double time_diff(struct timeval *prior, struct timeval *latter);
void measure_time(struct timeval** start, struct timeval** current, int incr);


int main(){
    struct timeval* start = NULL, *current = NULL;
    while(1){
        measure_time(&start, &current, 0);
    }

}

void measure_time(struct timeval** start, struct timeval** current, int incr){

    static int count = 0;
    const int SECONDS = 1;
    const int MILIS = SECONDS * 1000;

    if (*start == NULL){
        *start = (struct timeval *) malloc(sizeof(struct timeval));
        *current = (struct timeval *) malloc(sizeof(struct timeval));
        gettimeofday(*start, NULL);
    }

    if (!incr) return;

    gettimeofday(*current, NULL);

    if (time_diff(*start, *current) >= MILIS){
        // printf("Measured for thread in pool %d - workload %d\n", args->thread_num, args->loop_num);
        printf("  throughput %.3Lf %d\n", (count/(time_diff(*start, *current) /1000)), count);
        count = 0;
        gettimeofday(*start, NULL);
    }else{
        count++;
    }
}


long double time_diff(struct timeval *prior, struct timeval *latter) {
    return (double) (latter->tv_usec - prior->tv_usec) / 1000.0L +
           (double) (latter->tv_sec - prior->tv_sec) * 1000.0L;
}